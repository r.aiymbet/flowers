package flowergrp

import (
	"database/sql"
	"github.com/gorilla/mux"
	"gitlab.com/r.aiymbet/flowers/helpers"
	"gitlab.com/r.aiymbet/flowers/internal/core/flower"
	"log"
	"net/http"
	"strconv"
)

type Handler struct {
	Log              *log.Logger
	FlowerRepository flower.Repository
}

func (h *Handler) GetFlower(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		helpers.RespondWithError(w, http.StatusBadRequest, "Invalid flowergrp ID")
		return
	}

	f := flower.Flower{ID: id}
	if err := f.GetFlower(sql.DB{}); err != nil {
		switch err {
		case sql.ErrNoRows:
			helpers.RespondWithError(w, http.StatusNotFound, "Flower not found")
		default:
			helpers.RespondWithError(w, http.StatusInternalServerError, "Something went wrong")
		}
		return
	}

	helpers.RespondWithJSON(w, http.StatusOK, f)
}

/*func (a *App) fetchFlowers(w http.ResponseWriter, r *http.Request) {
	count, _ := strconv.Atoi(r.FormValue("count"))
	start, _ := strconv.Atoi(r.FormValue("start"))

	if count > 10 || count < 1 {
		count = 10
	}
	if start < 0 {
		start = 0
	}

	flowers, err := fetchFlowers(a.DB, start, count)
	if err != nil {
		helpers.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	helpers.RespondWithJSON(w, http.StatusOK, flowers)
}

func (a *App) createFlower(w http.ResponseWriter, r *http.Request) {
	var f flowergrp
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&f); err != nil {
		helpers.RespondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	defer r.Body.Close()

	if err := f.createFlower(a.DB); err != nil {
		helpers.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	helpers.RespondWithJSON(w, http.StatusCreated, f)
}

func (a *App) updateFlower(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		helpers.RespondWithError(w, http.StatusBadRequest, "Invalid flowergrp ID")
		return
	}

	var f flowergrp
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&f); err != nil {
		helpers.RespondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	defer r.Body.Close()
	f.ID = id

	if err := f.updateFlower(a.DB); err != nil {
		helpers.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	helpers.RespondWithJSON(w, http.StatusOK, f)
}

func deleteFlower(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		helpers.RespondWithError(w, http.StatusBadRequest, "Invalid flowergrp ID")
		return
	}

	f := flowergrp{ID: id}
	if err := f.deleteFlower(a.DB); err != nil {
		helpers.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	helpers.RespondWithJSON(w, http.StatusOK, map[string]string{"result": "success"})
}*/

/*func (a *App) InitializeRoutes() {
	a.Router.HandleFunc("/flowers", a.fetchFlowers).Methods("GET")
	a.Router.HandleFunc("/flowergrp", a.createFlower).Methods("POST")
	a.Router.HandleFunc("/flowergrp/{id:[0-9]+}", a.getFlower).Methods("GET")
	a.Router.HandleFunc("/flowergrp/{id:[0-9]+}", a.updateFlower).Methods("PUT")
	a.Router.HandleFunc("/flowergrp/{id:[0-9]+}", a.deleteFlower).Methods("DELETE")
}*/
