package handlers

import (
	"github.com/gorilla/mux"
	flowergrp "gitlab.com/r.aiymbet/flowers/cmd/flowers-api/handlers/flowergrp"
	"gitlab.com/r.aiymbet/flowers/internal/container"
	"log"
)

type App struct {
	*mux.Router
	log *log.Logger
}

func NewApp(router *mux.Router, log *log.Logger) *App {
	return &App{
		Router: router,
		log:    log,
	}
}

func (a *App) Route() {
	router := mux.NewRouter()
	flowerHandler := flowergrp.Handler{
		Log:              a.log,
		FlowerRepository: container.FlowerRepository(),
	}

	router.HandleFunc("/flowers", flowerHandler.GetFlower).Methods("GET")

	//userHandler := usergrp.Handler{}
	//router.HandleFunc("/user", userHandler.GetUser).Methods("GET")
}
