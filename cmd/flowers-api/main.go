package main

import (
	"github.com/gorilla/mux"
	"gitlab.com/r.aiymbet/flowers/cmd/flowers-api/handlers"
	"log"
	"net/http"
	"os"
)

func main() {
	log := log.New(os.Stdout, "FLOWERS-API : ", log.LstdFlags|log.Lmicroseconds|log.Lshortfile)
	app := handlers.NewApp(mux.NewRouter(), log)
	app.Route()

	srv := &http.Server{
		Addr:    os.Getenv("API_ADDR"),
		Handler: app,
	}

	err := srv.ListenAndServe()
	if err != nil {
		return
	}
	select {}
}
