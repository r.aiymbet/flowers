module gitlab.com/r.aiymbet/flowers

go 1.16

require (
	github.com/elastic/go-elasticsearch/v8 v8.0.0-20211029144209-b121d5c06a2f // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/lib/pq v1.10.3 // indirect
)
