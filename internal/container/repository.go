package container

import (
	"database/sql"
	_ "github.com/lib/pq"
	"gitlab.com/r.aiymbet/flowers/internal/core/flower"
	"gitlab.com/r.aiymbet/flowers/internal/repository/postgres"
	"log"
	"os"
)

var db *sql.DB

func Database() *sql.DB {
	os.Setenv("POSTGRES_DSN", "postgres://flower_db:qwerty@localhost/flower_db?sslmode=disable")
	if db == nil {
		dsn := os.Getenv("POSTGRES_DSN")
		if dsn == "" {
			panic("postgres dsn is empty")
		}
		var err error
		db, err = sql.Open("postgres", dsn)
		if err != nil {
			log.Panicf("error dbConnect : %v", err)
		}
	}

	return db
}

var flowerRepository flower.Repository

func FlowerRepository() flower.Repository {
	if flowerRepository == nil {
		flowerRepository = postgres.NewFlowerRepository(Database())
	}
	return flowerRepository
}
