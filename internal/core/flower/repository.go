package flower

type Repository interface {
	Create(f *Flower) error
	Get(f *Flower) (*Flower, error)
	GetAll() ([]*Flower, error)
	Update(f *Flower) error
	Delete(f *Flower) error
}
