package postgres

import (
	"database/sql"
	"gitlab.com/r.aiymbet/flowers/internal/core/flower"
)

type FlowerModel struct {
	db *sql.DB
}

func NewFlowerRepository(db *sql.DB) flower.Repository {
	return &FlowerModel{
		db: db,
	}
}

func (m *FlowerModel) Create(f *flower.Flower) error {

	return nil
}

func (m *FlowerModel) Get(f *flower.Flower) (*flower.Flower, error) {

	return nil, nil
}

func (m *FlowerModel) GetAll() ([]*flower.Flower, error) {

	return nil, nil
}

func (m *FlowerModel) Update(f *flower.Flower) error {

	return nil
}

func (m *FlowerModel) Delete(f *flower.Flower) error {

	return nil
}
